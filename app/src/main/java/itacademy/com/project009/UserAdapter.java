package itacademy.com.project009;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

public class UserAdapter extends ArrayAdapter<UserModel> {

    private Context context;
    private SQLiteHelper sqLiteHelper;

    public UserAdapter(Context context, ArrayList<UserModel> list, SQLiteHelper sqLiteHelper) {
        super(context, 0, list);
        this.context = context;
        this.sqLiteHelper = sqLiteHelper;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.user_list_item, parent, false);
            holder = new ViewHolder();
            holder.tvName = convertView.findViewById(R.id.tvName);
            holder.tvNumber = convertView.findViewById(R.id.tvNumber);
            holder.checkBox = convertView.findViewById(R.id.checkBox);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final UserModel model = getItem(position);

        holder.tvName.setText(model.getName());
        holder.tvNumber.setText(String.valueOf(model.getNumber()));
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            }
        });

        return convertView;
    }

    private class ViewHolder {
        TextView tvName, tvNumber;
        CheckBox checkBox;
    }

}
