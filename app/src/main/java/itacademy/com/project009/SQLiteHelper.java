package itacademy.com.project009;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class SQLiteHelper extends SQLiteOpenHelper {
    private final static String DB_NAME = "PROJECT09";
    private final static int DB_VERSION = 2;

    private final static String TABLE_NAME = "USER_TABLE";
    private final static String ID = "_id";
    private final static String USER_ID = "USER_ID";
    private final static String USER_NAME = "USER_NAME";
    private final static String USER_NUMBER = "USER_NUMBER";

    private final static String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME + "(" +
            ID + " INTEGER_PRIMARY_KEY, " +
            USER_ID + " INTEGER, " +
            USER_NAME + " TEXT, " +
            USER_NUMBER + " INTEGER" +
            ");";


    public SQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void saveUserList(ArrayList<UserModel> userList) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();

        for (int i = 0; i < userList.size(); i++) {
            UserModel model = userList.get(i);
            cv.put(USER_ID, model.getId());
            cv.put(USER_NAME, model.getName());
            cv.put(USER_NUMBER, model.getNumber());

            long rowId = db.insert(TABLE_NAME, null, cv);
            Log.d("ROW_INSERTED", "ROWID = " + rowId);
        }
        db.close();
    }

    public void saveSingleModel(UserModel model) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(USER_ID, model.getId());
        cv.put(USER_NAME, model.getName());
        cv.put(USER_NUMBER, model.getNumber());

        long rowId = db.insert(TABLE_NAME, null, cv);
        Log.d("CREATE", "ROW_INSERTED " + rowId);
        db.close();
    }

    public void deleteSingleModel(int id) {
        SQLiteDatabase db = getWritableDatabase();
        long rowId = db.delete(TABLE_NAME, USER_ID + " = " + id, null);
        Log.d("DB", "MODEL IS DELETED " + id);
        db.close();
    }

    public void deleteList() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.close();
    }

}
