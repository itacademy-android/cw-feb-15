package itacademy.com.project009;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private UserAdapter adapter;
    private ArrayList<UserModel> userList;
    private SQLiteHelper sqLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnDelete = findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(this);

        sqLiteHelper = new SQLiteHelper(this);

        ListView listView = findViewById(R.id.listView);

        userList = new ArrayList<>();
        for (int i = 10; i < 30; i++) {
            UserModel model = new UserModel();
            model.setId(i);
            model.setName("John Smith " + i);
            model.setNumber(99995555);
            userList.add(model);
        }

        sqLiteHelper.saveUserList(userList);
        adapter = new UserAdapter(this, userList, sqLiteHelper);
        listView.setAdapter(adapter);

        View headerView = getLayoutInflater().inflate(R.layout.listview_footer, null);
        listView.addHeaderView(headerView);

        TextView tvHeader = headerView.findViewById(R.id.tvHeader);
        String amount = String.format("Amount: %s", userList.size());
        tvHeader.setText(amount);

    }

    @Override
    public void onClick(View view) {
        sqLiteHelper.deleteList();
    }
}
